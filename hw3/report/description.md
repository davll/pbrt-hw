### Description of implementation approach and comments

我建立一個 class `MedianCutIteration`，負責進行 median cut 計算。
首先建立 summed area table，然後跑遞迴，以O(n)方式找出分割線，繼續分割下去，到 maximum 
levels 為止 (`max levels = log2(nSamples)`)。
然後在遞迴分割的 leaf 端，以 region 的中心點作為 point light，area of region 就當作 
light intensity。
還有一點，point lights 是假設放在 unit sphere 表面上，以 Light space 為坐標系。

在 `MedianCutEnvironmentLight` 會在 constructor 執行這個 median cut 計算，儲存 
point lights，用作接下來的計算。

既然`MedianCutEnvironmentLight`把整個 light 假想成多個 virtual point lights，且放在
無限大的 sphere 上（雖然坐標上在 unit sphere 上），所以基本上可以用 PointLight 的程式碼，
但必須修改做大量改寫以滿足 Environment Light 的條件。

首先，第一個問題是，現在考慮的 Lights 都是離散的 virtual point lights, 而不是連續的 
environment map，但問題是 pbrt 的架構上並不允許多個discrete lights 在同一個時間點上
計算，所以只好利用 LightSample 去"取樣" light ID 而不是 position 。雖然這個方法不是
正確的，但至少有提供一個可行的 work around。

接下來第二個問題是，Environment Light是假定在無限大的sphere上，但 PointLight 的計算
是有考慮到距離。我基本上會把他們的 visibility 修改成以 ray 來處理（而非 segment），並不
考慮隨距離而衰減的因素，而且在 SHProject 跑 for 迴圈把個別的 lights 疊合在一起。

最後，render result 會有嚴重過亮問題，推測是 sample 方式的關係，變成 lights 重複打光好幾次了
我們最後除了用 Photoshop 調 exposure 以外還有依據 nSample 作調整 (講白點是 hack)

看著這些結果，light 數量越多，結果就越正確。

附註：感謝與黃偉寧同學討論作業
