
/*
    pbrt source code Copyright(c) 1998-2012 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_SHAPES_HEIGHTFIELD2_H
#define PBRT_SHAPES_HEIGHTFIELD2_H

// shapes/heightfield2.h*
#include "shape.h"
#include "heightfield.h"

// Heightfield2 Declarations
class Heightfield2 : public Heightfield {
public:
    // Heightfield2 Public Methods
    Heightfield2(const Transform *o2, const Transform *w2o, bool ro, int nu, int nv, const float *zs);
    virtual ~Heightfield2();
    bool CanIntersect() const;
    bool Intersect(const Ray &ray, float *tHit,
                   float *rayEpsilon, DifferentialGeometry *dg) const;
    bool IntersectP(const Ray &ray) const;
protected:
  BBox bounds;
  Vector cell_width, cell_width_inversed;
  
  bool cell_is_valid(int x, int y) const {
    return (x>=0 && x<nx-1)&&(y>=0 && y<ny-1);
  }
  
  int pos_to_cell(const Point& p, int axis) const {
    return Float2Int((p[axis]-bounds.pMin[axis])*cell_width_inversed[axis]);
  }
  
  float cell_to_pos(int p, int axis) const {
    return bounds.pMin[axis] + p * cell_width[axis];
  }
  
  float z00_of_cell(int x, int y) const { return z[ x + y * nx ]; }
  
  struct Cell{
    // Bilinear Quadratic Parameters
    Vector a, b, c, d;
    
    const Point p(float u, float v) const {
      Vector vec = a*(u*v) + b*u + c*v + d;
      return Point(vec.x, vec.y, vec.z);
    }
    
    const Vector dpdu(float u, float v) const {
      return a*v + b;
    }
    
    const Vector dpdv(float u, float v) const {
      return a*u + c;
    }
    
    const Vector d2pdu2(float u, float v) const {
      return Vector(0,0,0);
    }
    
    const Vector d2pdudv(float u, float v) const {
      return a;
    }
    
    const Vector d2pdv2(float u, float v) const {
      return Vector(0,0,0);
    }
    
    const Vector d2pdvdu(float u, float v) const {
      return a;
    }
    
    // Bounding box of the surface cell
    BBox box;
    
    bool inside(const Point& pt)const{
      Point p2 = pt;
      p2.z = (box.pMin.z + box.pMax.z) * 0.5f;
      return box.Inside(p2);
    }
    
    bool intersect(const Ray& ray)const{
      Ray r2 = ray;
      r2.o.z = (box.pMin.z + box.pMax.z) * 0.5f;
      r2.d.z = 0.0f;
      return box.IntersectP(r2);
    }
    
    // Bilinear Patch Equation
    // Axy + Bx + Cy + D = z
    float eq_a, eq_b, eq_c, eq_d;
    
    // Bilinear Quadratic Parameters for dpdu (x-gradient)
    Vector gx_a, gx_b, gx_c, gx_d;
    
    const Vector gx(float u, float v) const {
      return gx_a*(u*v) + gx_b*u + gx_c*v + gx_d;
    }
    
    // Bilinear Quadratic Parameters for dpdv (y-gradient)
    Vector gy_a, gy_b, gy_c, gy_d;
    
    const Vector gy(float u, float v) const {
      return gy_a*(u*v) + gy_b*u + gy_c*v + gy_d;
    }
    
  };
  
  const Cell* intersect(const Ray& world_ray, float& hitT) const ;
  
  Cell* cells;
  
  Cell& get_cell(int x, int y) const {
    return this->cells[ x + y * (nx-1) ];
  }
  
};


Heightfield2 *CreateHeightfield2Shape(const Transform *o2w, const Transform *w2o,
        bool reverseOrientation, const ParamSet &params);

#endif // PBRT_SHAPES_HEIGHTFIELD2_H
