
/*
    pbrt source code Copyright(c) 1998-2012 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */


// shapes/heightfield2.cpp*
#include "stdafx.h"
#include "shapes/heightfield2.h"
#include "shapes/trianglemesh.h"
#include "paramset.h"

#include <math.h>

// Heightfield2 Method Definitions
Heightfield2::Heightfield2(const Transform *o2w, const Transform *w2o,
        bool ro, int x, int y, const float *zs)
: Heightfield(o2w, w2o, ro, x, y, zs)
{
  const int width = nx-1, height = ny-1;
  
  cell_width = Vector(1.0f/width, 1.0f/height, 0.0f);
  cell_width_inversed = Vector(width, height, 0.0f);
  
  cells = new Cell[width*height];
  
  bounds = Heightfield::ObjectBound();
  
  // Initialize Cells
  for(int i=0; i<height; ++i)
    for(int j=0; j<width; ++j)
    {
      const float cx0 = cell_to_pos(j, 0);
      const float cx1 = cell_to_pos(j+1, 0);
      const float cy0 = cell_to_pos(i, 1);
      const float cy1 = cell_to_pos(i+1, 1);
      
      Cell& cell = get_cell(j,i);
      
      Point p00(cx0, cy0, z00_of_cell(j,i));
      Point p10(cx1, cy0, z00_of_cell(j+1,i));
      Point p01(cx0, cy1, z00_of_cell(j,i+1));
      Point p11(cx1, cy1, z00_of_cell(j+1,i+1));
      
      cell.box = Union(BBox(p00, p11), BBox(p10, p01));
      cell.a = (p11 - p10) + (p00 - p01);
      cell.b = p10 - p00;
      cell.c = p01 - p00;
      cell.d = Vector(p00);
      
      // a.x = 0, a.y = 0
      // b.y = 0
      // c.x = 0
      
      if (false) { // dump
        printf("===== Cell x=%d, y=%d =====\n", j, i);
        printf("a = %f %f %f\n", cell.a.x, cell.a.y, cell.a.z);
        printf("b = %f %f %f\n", cell.b.x, cell.b.y, cell.b.z);
        printf("c = %f %f %f\n", cell.c.x, cell.c.y, cell.c.z);
        printf("d = %f %f %f\n", cell.d.x, cell.d.y, cell.d.z);
        printf("===========================\n");
      }
      
      float eq_n = cell.b.x * cell.c.y;
      float eq_a = cell.a.z;
      float eq_b = cell.b.z * cell.c.y - cell.d.y * cell.a.z;
      float eq_c = cell.c.z * cell.b.x - cell.d.x * cell.a.z;
      float eq_d = cell.d.x * cell.d.y * cell.a.z + 
                   cell.b.x * cell.c.y * cell.d.z - 
                   cell.b.x * cell.d.y * cell.c.z - 
                   cell.d.x * cell.c.y * cell.b.z ;
      eq_a /= eq_n, eq_b /= eq_n, eq_c /= eq_n, eq_d /= eq_n;
      
      cell.eq_a = eq_a;
      cell.eq_b = eq_b;
      cell.eq_c = eq_c;
      cell.eq_d = eq_d;
      
      float g00, g01, g10, g11;
      Vector gv00, gv01, gv10, gv11;
      
      // X-gradient
      if(j == 0)
        g00 = 0.0f, g01 = 0.0f;
      else
        g00 = (z00_of_cell(j+1, i) - z00_of_cell(j-1, i)) * 0.5f,
        g01 = (z00_of_cell(j+1, i+1) - z00_of_cell(j-1, i+1)) * 0.5f;
      
      if (j == width-1)
        g10 = 0.0f, g11 = 0.0f;
      else
        g10 = (z00_of_cell(j+2, i) - z00_of_cell(j, i)) * 0.5f,
        g11 = (z00_of_cell(j+2, i+1) - z00_of_cell(j, i+1)) * 0.5f;
      
      gv00 = Vector(1.0f, 0.0f, g00 * cell_width_inversed.x);
      gv01 = Vector(1.0f, 0.0f, g01 * cell_width_inversed.x);
      gv10 = Vector(1.0f, 0.0f, g10 * cell_width_inversed.x);
      gv11 = Vector(1.0f, 0.0f, g11 * cell_width_inversed.x);
      
      cell.gx_a = (gv11 - gv10) + (gv00 - gv01);
      cell.gx_b = gv10 - gv00;
      cell.gx_c = gv01 - gv00;
      cell.gx_d = gv00;
      
      // Y-gradient
      if(i == 0)
        g00 = 0.0f, g10 = 0.0f;
      else
        g00 = (z00_of_cell(j, i+1) - z00_of_cell(j, i-1)) * 0.5f,
        g10 = (z00_of_cell(j+1, i+1) - z00_of_cell(j+1, i-1)) * 0.5f;
      
      if (i == height-1)
        g01 = 0.0f, g11 = 0.0f;
      else
        g01 = (z00_of_cell(j, i+2) - z00_of_cell(j, i)) * 0.5f,
        g11 = (z00_of_cell(j+1, i+2) - z00_of_cell(j+1, i)) * 0.5f;
      
      gv00 = Vector(0.0f, 1.0f, g00 * cell_width_inversed.y);
      gv01 = Vector(0.0f, 1.0f, g01 * cell_width_inversed.y);
      gv10 = Vector(0.0f, 1.0f, g10 * cell_width_inversed.y);
      gv11 = Vector(0.0f, 1.0f, g11 * cell_width_inversed.y);
      
      cell.gy_a = (gv11 - gv10) + (gv00 - gv01);
      cell.gy_b = gv10 - gv00;
      cell.gy_c = gv01 - gv00;
      cell.gy_d = gv00;
      
    }
}


Heightfield2::~Heightfield2() {
  delete[] cells;
}


bool Heightfield2::CanIntersect() const {
    return true;
}

const Heightfield2::Cell* Heightfield2::intersect
  (const Ray& world_ray, float& hitT) const
{
  // Transform the ray from world space to object space
  Ray ray;
  (*WorldToObject)(world_ray, &ray);
  
  // Find starting point
  float ray_t;
  {
    if (bounds.Inside(ray(ray.mint)))
      ray_t = ray.mint;
    else if (!bounds.IntersectP(ray, &ray_t))
      return NULL;
  }
  
  const Point startPt = ray(ray_t);
  
  // Setup 2D-DDA
  float next_t[2], delta_t[2];
  int step[2], cell_index[2];
  for (int axis = 0; axis < 2; axis++) {
    cell_index[axis] = pos_to_cell(startPt, axis);
    if (ray.d[axis] >= 0.0f) {
      next_t[axis] = ray_t + 
        (cell_to_pos(cell_index[axis]+1, axis) - startPt[axis]) / ray.d[axis];
      delta_t[axis] = cell_width[axis] / ray.d[axis];
      step[axis] = 1;
    } else {
      next_t[axis] = ray_t + 
        (cell_to_pos(cell_index[axis], axis) - startPt[axis]) / ray.d[axis];
      delta_t[axis] = -cell_width[axis] / ray.d[axis];
      step[axis] = -1;
    }
  }
  
  // Walk ray through cells
  for (;;) {
    if ((cell_index[0] < 0 || cell_index[0] > nx-2)||
        (cell_index[1] < 0 || cell_index[1] > ny-2))
      break;
    
    const Cell& cell = get_cell(cell_index[0], cell_index[1]);
    
    if (cell.intersect(ray))
    {
      // 
      float eq_p = cell.eq_a * ray.d.x * ray.d.y;
      float eq_q = cell.eq_a * ( ray.o.x * ray.d.y + ray.o.y * ray.d.x ) +
                   cell.eq_b * ray.d.x + cell.eq_c * ray.d.y - ray.d.z ;
      float eq_r = cell.eq_a * ray.o.x * ray.o.y + 
                   cell.eq_b * ray.o.x + 
                   cell.eq_c * ray.o.y - 
                   ray.o.z + 
                   cell.eq_d ;
      
      // Solve it
      float sol;
      bool valid_sol = false;
      
      if (fabsf(eq_p) == 0.0f) {
        sol = - eq_r / eq_q;
        if (sol >= ray.mint && sol <= ray.maxt) {
          valid_sol = cell.inside(ray(sol));
        }
      } else {
        const float big_d = eq_q * eq_q - 4.0f * eq_p * eq_r;
        
        if(big_d < 0.0f)
          valid_sol = false;
        else {
          const float sqr_d = sqrtf(big_d);
          float t[] = {
            (-eq_q + sqr_d) / (2.0f * eq_p),
            (-eq_q - sqr_d) / (2.0f * eq_p)
          };
          bool t_ok[] = { false, false };
          
          for(int i=0;i<2;i++)
            if (t[i] >= ray.mint && t[i] <= ray.maxt)
              t_ok[i] = cell.inside(ray(t[i]));
          if (t_ok[0] && t_ok[1])
            sol = fminf(t[0], t[1]), valid_sol = true;
          else if (t_ok[0])
            sol = t[0], valid_sol = true;
          else if (t_ok[1])
            sol = t[1], valid_sol = true;
        }
      }
      
      // Return YES if the solution is valid
      if (valid_sol) {
        hitT = sol;
        return &cell;
      }
      
      //END
    }
    
    // advance to next cell
    const int step_axis = ((next_t[0] < next_t[1])? 0 : 1 );
    if (ray.maxt < next_t[step_axis])
      break;
    
    ray_t += delta_t[step_axis];
    cell_index[step_axis] += step[step_axis];
    next_t[step_axis] += delta_t[step_axis];
  }
  
  // No Intersection
  return NULL;
}

bool Heightfield2::Intersect(const Ray &world_ray, float *tHit,
                   float *rayEpsilon, DifferentialGeometry *dg) const {
  float hitT;
  const Cell* cell = this->intersect(world_ray, hitT);
  
  if (cell != NULL) {
    // Fill in parameters
    *tHit = hitT;
    *rayEpsilon = 5e-4f * hitT;
    
    Ray ray;
    (*WorldToObject)(world_ray, &ray);
    
    const Point hitP = ray(hitT);
    const float u = hitP.x;
    const float v = hitP.y;
    const float ub = (u - cell->box.pMin.x) * cell_width_inversed.x;
    const float vb = (v - cell->box.pMin.y) * cell_width_inversed.y;
    //Vector dpdu = Vector(cell_width.x, 0, 0);
    //Vector dpdv = Vector(0, cell_width.y, 0);
    Point p = cell->p(ub, vb);
    //Vector dpdu = cell->dpdu(ub, vb) * cell_width_inversed.x;
    //Vector dpdv = cell->dpdv(ub, vb) * cell_width_inversed.y;
    //Vector d2pdu2 = cell->d2pdu2(ub, vb) * 
    //                cell_width_inversed.x * cell_width_inversed.x;
    //Vector d2pdv2 = cell->d2pdv2(ub, vb) * 
    //                cell_width_inversed.y * cell_width_inversed.y;
    //Vector d2pdudv = cell->d2pdudv(ub, vb) * 
    //                 cell_width_inversed.x * cell_width_inversed.y;
    //Vector d2pdvdu = cell->d2pdvdu(ub, vb) * 
    //                 cell_width_inversed.x * cell_width_inversed.y;
    //Normal dndu = Normal(Cross(d2pdu2, dpdv) + Cross(dpdu, d2pdvdu));
    //Normal dndv = Normal(Cross(d2pdudv, dpdv) + Cross(dpdu, d2pdv2));
    
    Vector dpdu = cell->gx(ub, vb);
    Vector dpdv = cell->gy(ub, vb);
    Normal dndu = Normal(0,0,0);
    Normal dndv = Normal(0,0,0);
    
    const Transform &o2w = *ObjectToWorld;
    *dg = DifferentialGeometry(o2w(p), o2w(dpdu), o2w(dpdv),
                               o2w(dndu), o2w(dndv), u, v, this);
  }
  
  return cell != NULL;
}

bool Heightfield2::IntersectP(const Ray &world_ray) const {
  float hitT;
  return this->intersect(world_ray, hitT) != NULL;
}

Heightfield2 *CreateHeightfield2Shape(const Transform *o2w, 
                                      const Transform *w2o,
                                      bool reverseOrientation, 
                                      const ParamSet &params) {
    int nu = params.FindOneInt("nu", -1);
    int nv = params.FindOneInt("nv", -1);
    int nitems;
    const float *Pz = params.FindFloat("Pz", &nitems);
    Assert(nitems == nu*nv);
    Assert(nu != -1 && nv != -1 && Pz != NULL);
    return new Heightfield2(o2w, w2o, reverseOrientation, nu, nv, Pz);
}


