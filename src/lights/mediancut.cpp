
/*
    pbrt source code Copyright(c) 1998-2012 Matt Pharr and Greg Humphreys.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */


// lights/infinite.cpp*
#include "stdafx.h"
#include "lights/mediancut.h"
#include "sh.h"
#include "montecarlo.h"
#include "paramset.h"
#include "imageio.h"

#include <math.h>
#include <vector>

// Median Cut Algorithm
class MedianCutIteration {
public:
  MedianCutIteration(RGBSpectrum* texels, int _width, int _height)
  : width(_width), height(_height)
  {
    sum_table = new RGBSpectrum[width * height];
    
    for (int y = 0; y < height; ++y) {
      const float theta = M_PI * float(y+.5f)/float(height);
      const float weight = sinf(theta);
      for (int x = 0; x < width; ++x)
        sum_table[x + width * y] = texels[x + width * y] * weight;
    }
    for (int x = 1; x < width; ++x)
      sum_table[x] += sum_table[x-1];
    for (int y = 1; y < height; ++y) {
      sum_table[width * y] += sum_table[width * (y-1)];
      for (int x = 1; x < width; ++x) {
        RGBSpectrum left = sum_table[x-1 + width * y];
        RGBSpectrum upper_left = sum_table[x-1 + width * (y-1)];
        RGBSpectrum upper = sum_table[x + width * (y-1)];
        sum_table[x + width * y] += left + upper - upper_left;
      }
    }
    
    printf("total: %f\n", total_area().y());
  }
  
  ~MedianCutIteration()
  {
    delete[] sum_table;
  }
  
  void run(int nsamples)
  {
    const int max_iteration = (int)log2f((float)nsamples);
    lights.clear();
    iterate(0, 0, width, height, 0, max_iteration);
  }
  
  RGBSpectrum total_area()const { return area_of_rgb(0,0,width,height); }
  
public:
  std::vector< std::pair<Point, RGBSpectrum> > lights;
  
private:
  void iterate(int x0, int y0, int w, int h, int lv, int limit)
  {
    if (lv == limit) {
      RGBSpectrum pc = area_of_rgb(x0, y0, w, h);
      const float phi = (w * 0.5f + x0) / float(width) * 2.0f * M_PI;
      const float theta = (h * 0.5f + y0) / float(height) * M_PI;
      printf("%d %d %d %d %f %f %f\n", x0, y0, w, h, phi, theta, pc.y());
      // Add Light
      const float costheta = cosf(theta), sintheta = sinf(theta);
      const float sinphi = sinf(phi), cosphi = cosf(phi);
      float px = sintheta * cosphi, py = sintheta * sinphi, pz = costheta;
      //printf("%f %f %f\n", px, py, pz);
      lights.push_back( std::make_pair(Point(px, py, pz), pc) );
    } else {
      float half_area = area_of_lum(x0, y0, w, h) * 0.5f;
      if (w > h) { // Split Horizontally
        for (int dx = 1; dx < w-1; ++dx)
          if (area_of_lum(x0, y0, dx, h) >= half_area) {
            iterate(x0, y0, dx, h, lv+1, limit);
            iterate(x0+dx, y0, w-dx, h, lv+1, limit);
            break;
          }
      } else { // Split Vertically
        for (int dy = 1; dy < h-1; ++dy)
          if (area_of_lum(x0, y0, w, dy) >= half_area) {
            iterate(x0, y0, w, dy, lv+1, limit);
            iterate(x0, y0+dy, w, h-dy, lv+1, limit);
            break;
          }
      }
    }
  }
  
  RGBSpectrum area_of_rgb(int x0, int y0, int w, int h)const
  {
    const int x1 = x0 + w - 1, y1 = y0 + h - 1;
    RGBSpectrum a = sum_table[x0 + width * y0];
    RGBSpectrum b = sum_table[x0 + width * y1];
    RGBSpectrum c = sum_table[x1 + width * y1];
    RGBSpectrum d = sum_table[x1 + width * y0];
    return c + a - b - d;
  }
  
  float area_of_lum(int x0, int y0, int w, int h)const
  {
    return area_of_rgb(x0, y0, w, h).y();
  }
  
private:
  const int width, height;
  RGBSpectrum* sum_table;
};

// MedianCutEnvironmentLight Method Definitions
MedianCutEnvironmentLight::~MedianCutEnvironmentLight() {
}

MedianCutEnvironmentLight::MedianCutEnvironmentLight
       (const Transform &light2world,
        const Spectrum &L, int ns, const string &texmap)
    : Light(light2world, ns)
{
    int width = 0, height = 0;
    RGBSpectrum *texels = NULL;
    // Read texel data from _texmap_ into _texels_
    if (texmap != "") {
        texels = ReadImage(texmap, &width, &height);
        if (texels)
            for (int i = 0; i < width * height; ++i)
                texels[i] *= L.ToRGBSpectrum();
    }
    if (!texels) {
        width = height = 1;
        texels = new RGBSpectrum[1];
        texels[0] = L.ToRGBSpectrum();
    }
    
    {
      MedianCutIteration miter(texels, width, height);
      miter.run(ns);
      this->point_lights = miter.lights;
      this->total_spectrum = miter.total_area();
    }
}


Spectrum MedianCutEnvironmentLight::Power(const Scene *scene) const {
    return Spectrum(total_spectrum, SPECTRUM_ILLUMINANT);
}

void MedianCutEnvironmentLight::SHProject(const Point &p, float pEpsilon,
        int lmax, const Scene *scene, bool computeLightVis,
        float time, RNG &rng, Spectrum *coeffs) const 
{
  for (int i = 0; i < SHTerms(lmax); ++i)
    coeffs[i] = 0.f;
  
  Point worldCenter;
  float worldRadius;
  scene->WorldBound().BoundingSphere(&worldCenter, &worldRadius);
  
  for (int light_id = 0; light_id < point_lights.size(); ++light_id) {
    Point lightPos = LightToWorld(point_lights[light_id].first);
    if (computeLightVis &&
        scene->IntersectP(Ray(p, Normalize(Vector(lightPos)), pEpsilon, 
                              INFINITY, time)))
      continue;
    // Project point light source to SH
    float *Ylm = ALLOCA(float, SHTerms(lmax));
    Vector wi = Normalize(Vector(lightPos));
    SHEvaluate(wi, lmax, Ylm);
    Spectrum Li = Spectrum(point_lights[light_id].second * nSamples / 128, SPECTRUM_ILLUMINANT);
    for (int i = 0; i < SHTerms(lmax); ++i)
      coeffs[i] += Li * Ylm[i];
  }
}


MedianCutEnvironmentLight *CreateMedianCutLight
  (const Transform &light2world, const ParamSet &paramSet)
{
    Spectrum L = paramSet.FindOneSpectrum("L", Spectrum(1.0));
    Spectrum sc = paramSet.FindOneSpectrum("scale", Spectrum(1.0));
    string texmap = paramSet.FindOneFilename("mapname", "");
    int nSamples = paramSet.FindOneInt("nsamples", 1);
    if (PbrtOptions.quickRender) nSamples = max(1, nSamples / 4);
    return new MedianCutEnvironmentLight(light2world, L * sc, nSamples, texmap);
}

int MedianCutEnvironmentLight::sample_light_id(const LightSample& ls)const
{
  return static_cast<int>(ls.uPos[0] * nSamples) % point_lights.size();
}

Spectrum MedianCutEnvironmentLight::Sample_L(const Point &p, float pEpsilon,
        const LightSample &ls, float time, Vector *wi, float *pdf,
        VisibilityTester *visibility) const 
{
  int light_id = sample_light_id(ls);
  //printf("dbg: %f %d\n", ls.uPos[0], light_id);
  Point lightPos = LightToWorld(point_lights[light_id].first);
  *wi = Normalize(Vector(lightPos));
  *pdf = 1.0f;
  visibility->SetRay(p, pEpsilon, *wi, time);
  return Spectrum(point_lights[light_id].second * nSamples / 128, SPECTRUM_ILLUMINANT);
}

float MedianCutEnvironmentLight::Pdf
(const Point &, const Vector &w) const
{
  return 0.0f;
}

Spectrum MedianCutEnvironmentLight::Sample_L
       (const Scene *scene,
        const LightSample &ls, float u1, float u2, float time,
        Ray *ray, Normal *Ns, float *pdf) const
{
  int light_id = sample_light_id(ls);
  Point lightPos = LightToWorld(point_lights[light_id].first);
  
  *ray = Ray(lightPos, -Normalize(Vector(lightPos)), 0.f, INFINITY, time);
  *Ns = (Normal)ray->d;
  *pdf = UniformSpherePdf();
  return Spectrum(point_lights[light_id].second * nSamples / 128, SPECTRUM_ILLUMINANT);
}


