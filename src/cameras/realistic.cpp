
#include "stdafx.h"
#include "cameras/realistic.h"
#include "montecarlo.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <string.h>
#include <unistd.h>

RealisticCamera::RealisticCamera(const AnimatedTransform &cam2world,
				 float hither, float yon, 
				 float sopen, float sclose, 
				 float filmdistance, float aperture_diameter, string specfile, 
				 float filmdiag, Film *f)
: Camera(cam2world, sopen, sclose, f),
  filmDist(filmdistance), apertureDiam(aperture_diameter), 
  clipHither(hither), clipYon(yon),
  filmZpos(-filmdistance)
{
  if (!parse_spec(specfile)) {
    std::cerr << "Error when parsing the lens spec file " << specfile << '\n';
    exit(1);
  }
  
  float scale = filmdiag / sqrtf(film->xResolution*film->xResolution + 
                                 film->yResolution*film->yResolution);
  float film_dx = film->xResolution * scale * 0.5f;
  float film_dy = film->yResolution * scale * 0.5f;
  
  // Assume the origin of the camera system is at the front lens
  RasterToCamera = Scale(-1,1,1) * // fix the issue of horizontal flipping
                   Translate(Vector(-film_dx, -film_dy, filmZpos)) * 
                   Scale(scale, scale, 1.0f);
}

float RealisticCamera::GenerateRay(const CameraSample &sample, Ray *ray) const
{
  // Calculate sample point
  Point p_rst(sample.imageX, sample.imageY, 0.0f);
  Point p_cam;
  RasterToCamera(p_rst, &p_cam);
  
  // Assume the exit pupil is at the back lens
  float lensU, lensV, lensZ, lensR;
  ConcentricSampleDisk(sample.lensU, sample.lensV, &lensU, &lensV);
  lensU *= surfaces.front().ap_radius;
  lensV *= surfaces.front().ap_radius;
  lensZ = surfaces.front().zpos;
  lensR = surfaces.front().ap_radius;
  
  // Bootstrap an eye ray
  {
    float epZ = sqrtf(lensR*lensR - lensU*lensU - lensV*lensV) + lensZ;
    ray->d = Normalize(Vector(lensU-p_cam.x, lensV-p_cam.y, epZ-p_cam.z));
    ray->o = p_cam;
    ray->time = Lerp(sample.time, shutterOpen, shutterClose);
  }
  
  // Trace the ray through all the lens surfaces
  Point p_hit;
  for (SurfaceVector::const_iterator it = surfaces.begin(); 
       it != surfaces.end(); ++it) {
    if (it->is_stop) {
      // Find intersection (ray-disk)
      float t = (it->center_z - ray->o.z) / ray->d.z;
      float x = ray->o.x + t * ray->d.x;
      float y = ray->o.y + t * ray->d.y;
      if (x * x + y * y > it->ap_radius2) return 0.0f;
    } else {
      // Find the intersection point, a.k.a. p_hit
      {
        Vector co = ray->o - Point(0.0f, 0.0f, it->center_z);
        float c = co.LengthSquared();
        float d = Dot(co, ray->d);
        float e = d * d - c + it->radius2;
        if (e < 0.0f) return 0.0f;
        float t = ((it->radius > 0.0f) ? (-d + sqrtf(e)) : (-d - sqrtf(e)));
        p_hit = ray->o + t * ray->d;
        if (p_hit.x*p_hit.x + p_hit.y*p_hit.y > it->ap_radius2)
          return 0.0f;
      }
      
      // get normal vector on the intersection point
      Vector normal;
      if (it->radius > 0.0f)
        normal = Normalize(Point(0.0f, 0.0f, it->center_z) - p_hit);
      else
        normal = Normalize(p_hit - Point(0.0f, 0.0f, it->center_z));
      
      Vector input = ray->d, output;
      
      // Heckber's method
      {
        float c1, c2;
        c1 = -Dot(input, normal);
        c2 = 1.0f - it->n_ratio2 * (1.0f - c1 * c1);
        if (c2 < 0.0f) return 0.0f;
        c2 = sqrtf(c2);
        output = it->n_ratio * input + (it->n_ratio * c1 - c2) * normal;
      }
      
      // next!
      ray->o = p_hit;
      ray->d = Normalize(output);
    }
  }
  
  // transform the eye ray to world space
  ray->mint = clipHither;
  ray->maxt = (clipYon - clipHither) / ray->d.z;
  CameraToWorld(*ray, ray);
  
  // BONUS: varying pixel weight
  float weight = Dot(Normalize(p_hit-p_cam),Vector(0,0,1));
  weight = weight * weight / fabsf(filmZpos);
  weight = weight * weight * (lensR*lensR * M_PI);
  
  return weight;
}

bool RealisticCamera::parse_spec(const string &specfile)
{
  std::string line;
  std::ifstream fin(specfile.c_str());
  
  if (!fin.is_open()) return false;
  
  surfaces.clear();
  
  float zpos = 0.0f;
  while (std::getline(fin, line)) {
    const char* line_ptr = line.c_str();
    if (line_ptr[0] == '#') // This is comment
      continue;
    float r, p, n, a;
    sscanf(line_ptr, "%f%f%f%f", &r, &p, &n, &a);
    surfaces.push_back(Surface(r,zpos,n,(n == 0.0f ? apertureDiam : a)));
    zpos -= p;
  }
  fin.close();
  filmZpos += zpos;
  
  if (surfaces.size() < 1) return false;
  
  std::reverse(surfaces.begin(), surfaces.end());
  
  for (SurfaceVector::iterator it = surfaces.begin(); 
       it != surfaces.end(); ++it)
  {
    if (it->is_stop)
      it->n_ratio = 1.0f;
    else {
      SurfaceVector::iterator it2 = it+1;
      if (it2 == surfaces.end() || it2->ndr == 0.0f)
        it->n_ratio = it->ndr;
      else
        it->n_ratio = it->ndr / it2->ndr;
    }
    it->n_ratio2 = it->n_ratio * it->n_ratio;
  }
  
  return true;
}

RealisticCamera *CreateRealisticCamera(const ParamSet &params,
        const AnimatedTransform &cam2world, Film *film) {
	// Extract common camera parameters from \use{ParamSet}
	float hither = params.FindOneFloat("hither", -1);
	float yon = params.FindOneFloat("yon", -1);
	float shutteropen = params.FindOneFloat("shutteropen", -1);
	float shutterclose = params.FindOneFloat("shutterclose", -1);

	// Realistic camera-specific parameters
	string specfile = params.FindOneString("specfile", "");
	float filmdistance = params.FindOneFloat("filmdistance", 70.0); // about 70 mm default to film
 	float fstop = params.FindOneFloat("aperture_diameter", 1.0);	
	float filmdiag = params.FindOneFloat("filmdiag", 35.0);

	Assert(hither != -1 && yon != -1 && shutteropen != -1 &&
		shutterclose != -1 && filmdistance!= -1);
	if (specfile == "") {
	    Severe( "No lens spec file supplied!\n" );
	}
	return new RealisticCamera(cam2world, hither, yon,
				   shutteropen, shutterclose, filmdistance, fstop, 
				   specfile, filmdiag, film);
}
