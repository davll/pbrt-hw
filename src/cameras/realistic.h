
#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_CAMERAS_REALISTIC_H
#define PBRT_CAMERAS_REALISTIC_H

#include "camera.h"
#include "paramset.h"
#include "film.h"
#include "sampler.h"
#include <vector>

// RealisticCamera Declarations
class RealisticCamera : public Camera {
public:
	// RealisticCamera Public Methods
	RealisticCamera(const AnimatedTransform &cam2world,
						float hither, float yon, float sopen,
						float sclose, float filmdistance, float aperture_diameter, string specfile,
						float filmdiag, Film *film);
	float GenerateRay(const CameraSample &sample, Ray *) const;
  
private:
	// RealisticCamera Private Methods
  
  struct Surface {
    bool is_stop;
    float radius, radius2; // of curvature
    float zpos; // of surface center
    float ndr; // 
    float ap_radius, ap_radius2; // aperture * 0.5
    float center_z; // of curvature sphere
    
    Surface(float r, float pos, float n, float apert)
    : is_stop(n==0.0f), radius(r), zpos(pos), ndr(n), ap_radius(apert * 0.5f)
    {
      radius2 = radius * radius;
      ap_radius2 = ap_radius * ap_radius;
      center_z = zpos - radius;
    }
    
    float n_ratio, n_ratio2; // n_in / n_out
  };
  
  typedef std::vector<Surface> SurfaceVector;
  
  // from image side to front
  SurfaceVector surfaces;
  
  bool parse_spec(const string &specfile);
  
  //
  Transform RasterToCamera;
  
  //
  float clipHither, clipYon;
  
  //
  float filmDist, apertureDiam, filmZpos;
  
};


RealisticCamera *CreateRealisticCamera(const ParamSet &params,
        const AnimatedTransform &cam2world, Film *film);


#endif	// PBRT_CAMERAS_REALISTIC_H
