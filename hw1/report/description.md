### Description of implementation approach and comments

首先，在 constructor 作 pre-processing，每個grid cell都要算出：

* Bounding Box，X與Y已知，而Z就分別取輸入的 height 之 local maximum & local minimum
* 每個cell之surface都是 Bilinear Patch，以 `V(u,v) = A*(u*v) + B*u + C*v + D` 描述，要算出各係數（請見附錄A）
* 求得上面的參數式後，把u,v消去轉換為Quadratic Equation: `aXY + Bx + Cy + D = z`
* 同理, 四個頂點各求出gradient (從輸入的height map求出difference再來轉成vector), 并轉換成 Bilinear Patch 之參數式 (如同附錄A)

之後每次作 Ray Intersection Test 時候，首先會檢查

* ray 之起點是否落在整個 heightfield 之 bounding box內，或者
* ray 與 heightfield 的 bounding box 相交，其交點當作 starting point

之後就作 X-Y 方向的 DDA (Digital Differential Analysis)，每遇到一個cell時候：

1. 檢查此次 ray 有無超出 heightfield，若超出則直接退出迴圈
2. 如果這個 ray 有與cell相交, 請繼續, 否則直接跳到 9
3. 把 ray 之參數式 與 此cell的 Quadratic Equation 結合，變成 `Pt^2 + Qt + R = 0`
4. 解方程式，分成幾種狀況: `P=0`, `Q^2-4PR >= 0`, 或者 `Q^2-4PR < 0`
5. `P=0`的情況，`t = - R / Q`, 而且要確認 `ray(t)`是有效的範圍且落在這個cell上
6. `Q^2-4PR < 0` 代表方程式無解
7. `Q^2-4PR >= 0`則有最多兩個根，請挑一個最小且確認`ray(t)`是有效的範圍且落在這個cell上
8. 如果 `t` 已經找到了而且是符合條件的，直接跳出迴圈並回傳 true (此cell)
9. 如果 ray 超出極限了，直接退出迴圈
10. DDA下一個階段
11. 回傳 false (NULL)

至於 Normal的計算，基本上是用 Bilinear Interpolation 作法：

    Vector dpdu = cell->gx(ub, vb); // X-gradient on u', v'
    Vector dpdv = cell->gy(ub, vb); // Y-gradient on u', v'
    Normal dndu = Normal(0,0,0);
    Normal dndv = Normal(0,0,0);

其中 `u', v'` 是cell上參數，要從heightfield的u,v轉換過來：

- `ub = (u - 此cell之左下角X座標) * cell_width.x`
- `vb = (v - 此cell之左下角Y座標) * cell_width.y`


#### 附錄A

參數式：

    V(u, v) = A * (u * v) + B * u + C * v + D
    
    A = p11 - p10 + p00 - p01
    B = p10 - p00
    C = p01 - p00
    D = p00

其中 p## 分別代表一個cell上頂點(vertex)座標

    v
    ^
    |
    |  p01 ---- p11
    |   |        |
    |   |        |
    |   |        |
    |  p00 ---- p10
    |
    L-----------------> u

